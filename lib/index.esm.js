import React from 'react';

function styleInject(css, ref) {
  if ( ref === void 0 ) ref = {};
  var insertAt = ref.insertAt;

  if (!css || typeof document === 'undefined') { return; }

  var head = document.head || document.getElementsByTagName('head')[0];
  var style = document.createElement('style');
  style.type = 'text/css';

  if (insertAt === 'top') {
    if (head.firstChild) {
      head.insertBefore(style, head.firstChild);
    } else {
      head.appendChild(style);
    }
  } else {
    head.appendChild(style);
  }

  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    style.appendChild(document.createTextNode(css));
  }
}

var css_248z = ".storybook-button {\r\n    font-family: 'Nunito Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;\r\n    font-weight: 700;\r\n    border: 0;\r\n    border-radius: 3em;\r\n    cursor: pointer;\r\n    display: inline-block;\r\n    line-height: 1;\r\n  }\r\n  .storybook-button--primary {\r\n    color: white;\r\n    background-color: #1ea7fd;\r\n  }\r\n  .storybook-button--secondary {\r\n    color: #333;\r\n    background-color: transparent;\r\n    box-shadow: rgba(0, 0, 0, 0.15) 0px 0px 0px 1px inset;\r\n  }\r\n  .storybook-button--small {\r\n    font-size: 12px;\r\n    padding: 10px 16px;\r\n  }\r\n  .storybook-button--medium {\r\n    font-size: 14px;\r\n    padding: 11px 20px;\r\n  }\r\n  .storybook-button--large {\r\n    font-size: 16px;\r\n    padding: 12px 24px;\r\n  }";
styleInject(css_248z);

/**
 * Primary UI component for user interaction
 */
var Button = function (_a) {
    var _b = _a.primary, primary = _b === void 0 ? true : _b, backgroundColor = _a.backgroundColor, _c = _a.size, size = _c === void 0 ? "medium" : _c, onClick = _a.onClick, label = _a.label;
    var mode = primary
        ? "storybook-button--primary"
        : "storybook-button--secondary";
    return (React.createElement("button", { type: "button", className: ["storybook-button", "storybook-button--" + size, mode].join(" "), style: backgroundColor ? { backgroundColor: backgroundColor } : {}, onClick: onClick }, label));
};

var Header = function (_a) {
    var label = _a.label;
    return (React.createElement(React.Fragment, null,
        React.createElement("h1", null, label)));
};

// import Image from '../img/dance.jpg';
var Monitor = function () {
    return (React.createElement(React.Fragment, null,
        React.createElement(Header, { label: "Monitor Page 1" }),
        React.createElement("img", { src: "../img/dance.jpg" }),
        React.createElement("h1", null, "Hello")));
};

export { Button, Monitor };
//# sourceMappingURL=index.esm.js.map
